package demo.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the position database table.
 * 
 */
@Entity
@NamedQuery(name="Position.findAll", query="SELECT p FROM Position p")
public class Position implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int pos_ID;

	private int allowance;

	private String pos_Name;

	public Position() {
	}

	public int getPos_ID() {
		return this.pos_ID;
	}

	public void setPos_ID(int pos_ID) {
		this.pos_ID = pos_ID;
	}

	public int getAllowance() {
		return this.allowance;
	}

	public void setAllowance(int allowance) {
		this.allowance = allowance;
	}

	public String getPos_Name() {
		return this.pos_Name;
	}

	public void setPos_Name(String pos_Name) {
		this.pos_Name = pos_Name;
	}

}