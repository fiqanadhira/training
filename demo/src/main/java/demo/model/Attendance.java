package demo.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**0
 * The persistent class for the attendance database table.
 * 
 */
@Entity
@Table(name="Attendance")
@NamedQuery(name="Attendance.findAll", query="SELECT a FROM Attendance a")
public class Attendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int att_ID;

	@Temporal(TemporalType.DATE)
	private Date att_Date;

	private String att_Status;

	private int emp_ID;

	public Attendance() {
	}

	public int getAtt_ID() {
		return this.att_ID;
	}

	public void setAtt_ID(int att_ID) {
		this.att_ID = att_ID;
	}

	public Date getAtt_Date() {
		return this.att_Date;
	}

	public void setAtt_Date(Date att_Date) {
		this.att_Date = att_Date;
	}

	public String getAtt_Status() {
		return this.att_Status;
	}

	public void setAtt_Status(String att_Status) {
		this.att_Status = att_Status;
	}

	public int getEmp_ID() {
		return this.emp_ID;
	}

	public void setEmp_ID(int emp_ID) {
		this.emp_ID = emp_ID;
	}

}