package demo.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the employees database table.
 * 
 */
@Entity
@Table(name="employees")
@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int emp_ID;

	private int age;

	private float emp_basic_sal;

	private String emp_Fname;

	private String emp_Lname;

	private String gender;

	private int pos_ID;

	public Employee() {
	}

	public int getEmp_ID() {
		return this.emp_ID;
	}

	public void setEmp_ID(int emp_ID) {
		this.emp_ID = emp_ID;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public float getEmp_basic_sal() {
		return this.emp_basic_sal;
	}

	public void setEmp_basic_sal(float emp_basic_sal) {
		this.emp_basic_sal = emp_basic_sal;
	}

	public String getEmp_Fname() {
		return this.emp_Fname;
	}

	public void setEmp_Fname(String emp_Fname) {
		this.emp_Fname = emp_Fname;
	}

	public String getEmp_Lname() {
		return this.emp_Lname;
	}

	public void setEmp_Lname(String emp_Lname) {
		this.emp_Lname = emp_Lname;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getPos_ID() {
		return this.pos_ID;
	}

	public void setPos_ID(int pos_ID) {
		this.pos_ID = pos_ID;
	}

}