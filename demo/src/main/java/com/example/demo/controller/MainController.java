package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;


import com.example.demo.model.Employees;
import com.example.demo.model.Position;
import com.example.demo.model.Attendance;
import com.example.demo.repository.EmployeesRepo;
import com.example.demo.repository.PositionRepo;
import com.example.demo.repository.AttendanceRepo;

@RestController
public class MainController {

	@Autowired
	private EmployeesRepo er;
	
	
	@GetMapping("/getempid")
	public Employees getByEmpId(@RequestParam Integer empId) {
		Employees employees = new Employees();
		employees = er.findByEmpID(empId);
		return employees;
	}

	@GetMapping("/getempposid")
	public List<Employees> getEmpPosId(@RequestParam Integer posID){
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByPosID(posID);
		return empls;
	}
	
	@GetMapping("/getempall")
	public List<Employees> getEmpAll(){
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findAll();
		return empls;
	}
	
 
	@GetMapping("/getgender")
	public List<Employees> getEmpGender(@RequestParam String gender){
		List<Employees> empgen = new ArrayList<Employees>();
		empgen = (List<Employees>) er.findByGender(gender);
		return empgen;
	}
	
	@GetMapping("/getage")
	public List<Employees> getEmpAge(@RequestParam Integer age){
		List<Employees> empage = new ArrayList<Employees>();
		empage = (List<Employees>) er.findByAge(age);
		return empage;
	}
	
	@PostMapping("/setemp")
	public Map<String, String> setEmp(@RequestBody Employees e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try{ 
			e = er.save(e);
			respMap.put("empid", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("empid", "");
			respMap.put("code",HttpStatus.CONFLICT+"");
			respMap.put("msg", "failed");
		}
		return respMap;
	}
	
	@PostMapping("/delete")
	public Map<String, String> delEmp(@RequestBody Employees e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try{ 
			er.delete(e);
			respMap.put("empid", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "success");
		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("empid", "");
			respMap.put("code",HttpStatus.CONFLICT+"");
			respMap.put("msg", "failed");
		}
		return respMap;
	}
	
	@Autowired
	private PositionRepo ar;
	
	@GetMapping("/getposall")
	public List<Position> getPosAll(){
		List<Position> position = new ArrayList<Position>();
		position = (List<Position>) ar.findAll();
		return position;
	}
	
	@GetMapping("/getposid")
	public Position getByPosId(@RequestParam Integer posID) {
		Position position = new Position();
		position = ar.findByPosID(posID);
		return position;
	}
	
	@GetMapping("/getposname")
	public Position getByPosName(@RequestParam String posName) {
		Position position = new Position();
		position = ar.findByPosName(posName);
		return position;
	}
	@GetMapping("/getallowance")
	public List<Position> getByAllowance(@RequestParam Integer allowance){
		List<Position> position = new ArrayList<Position>();
		position = (List<Position>) ar.findByAllowance(allowance);
		return position; 
	}
	
	@PostMapping("setpos")
	public Map<String, String> setPos(@RequestBody Position p)
	{
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			p = ar.save(p);
			
			respMap.put("empID", p.getPosID(). toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "Success");
			
		} catch (Exception e2){
			//TODO: handle exception
			respMap.put("empID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "Failed");
		}
		return respMap;
	}
	
	@PostMapping("delpos")
	public Map<String, String> delPos(@RequestBody Position p)
	{
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			 ar.delete(p);
			
			respMap.put("empID", p.getPosID(). toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "Success");
			
		} catch (Exception e2){
			//TODO: handle exception
			respMap.put("empID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "Failed");
		}
		return respMap;
	}
	
	@Autowired
	private AttendanceRepo ap;
	
	@GetMapping("/getattid")
	public Attendance getByAttenId(@RequestParam Integer attenId) {
		Attendance attendance = new Attendance();
		attendance = ap.findByAttenId(attenId);
		return attendance;
	}
	
	@GetMapping("/getattempid")
	public List<Attendance> getAttEmpId(@RequestParam Integer empId){
		List<Attendance> attnd = new ArrayList<Attendance>();
		attnd = (List<Attendance>) ap.findByEmpID(empId);
		return attnd; 
	}
	
	@GetMapping("/getattdate")
	public List<Attendance> getAttDate(@RequestParam Date attDate){
		List<Attendance> attnd = new ArrayList<Attendance>();
		attnd = (List<Attendance>) ap.findByAttDate(attDate);
		return attnd; 
	}
	
	@GetMapping("/getattstatus")
	public List<Attendance> getAttStatus(@RequestParam String attStatus){
		List<Attendance> attnd = new ArrayList<Attendance>();
		attnd = (List<Attendance>) ap.findByAttStatus(attStatus);
		return attnd; 
	}
	
	@GetMapping("/getattall")
	public List<Attendance> getAttAll(){
		List<Attendance> attnd = new ArrayList<Attendance>();
		attnd = (List<Attendance>) ap.findAll();
		return attnd;
	}
	
	@PostMapping("setatt")
	public Map<String, String> setAtt(@RequestBody Attendance a)
	{
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			a = ap.save(a);
			
			respMap.put("empID", a.getAttenId(). toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "Success");
			
		} catch (Exception e2){
			//TODO: handle exception
			respMap.put("empID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "Failed");
		}
		return respMap;
	}
	
	@PostMapping("delatt")
	public Map<String, String> delAtt(@RequestBody Attendance a)
	{
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			ap.delete(a);
			
			respMap.put("empID", a.getAttenId(). toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "Success");
			
		} catch (Exception e2){
			//TODO: handle exception
			respMap.put("empID", "");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "Failed");
		}
		return respMap;
	}
	
}
