package com.example.demo.repository;
import demo.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepo extends CrudRepository<Employee, Integer>{
	public Employee findByEmpId(Integer emp_ID);
}
