package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import java.util.List;

import com.example.demo.model.Employees;

public interface EmployeesRepo extends  CrudRepository<Employees, Integer> {

	public Employees findByEmpID(Integer empId);
	public List<Employees> findByPosID(Integer posID);
	public List<Employees> findByGender(String gender);
	public List<Employees> findByAge(Integer age);
}



	
