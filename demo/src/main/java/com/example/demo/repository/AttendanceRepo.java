package com.example.demo.repository;
import java.sql.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.example.demo.model.Attendance;


public interface AttendanceRepo extends CrudRepository<Attendance, Integer>{

	public Attendance findByAttenId(Integer attenId);
	public List<Attendance> findByEmpID(Integer empId);
	public List<Attendance> findByAttDate(Date attDate);
	public List<Attendance> findByAttStatus(String attStatus);
	public List<Attendance> findByAttDate(java.util.Date attDate);
}
