package com.example.demo.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.example.demo.model.Position;

public interface PositionRepo extends CrudRepository<Position, Integer> {

	public Position findByPosID(Integer posID);
	public Position findByPosName(String posName);
	public List<Position> findByAllowance(Integer allowance);
}



	
