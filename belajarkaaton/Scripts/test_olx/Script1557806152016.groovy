import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.olx.co.id/')

WebUI.click(findTestObject('Object Repository/OLX/Page_OLX - Cara Tepat Jual Cepat/strong_Akun Saya'))

WebUI.click(findTestObject('Object Repository/OLX/Page_OLX - Cara Tepat Jual Cepat/label_Biarkan saya tetap masuk'))

WebUI.setText(findTestObject('Object Repository/OLX/Page_OLX - Cara Tepat Jual Cepat/input_E-mail_loginemail'), 'fiqanadhira@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/OLX/Page_OLX - Cara Tepat Jual Cepat/input_Password_loginpassword'), 
    'WU3NcOp+YFF8C+TSkOh9xg==')

WebUI.click(findTestObject('Object Repository/OLX/Page_OLX - Cara Tepat Jual Cepat/input_Biarkan saya tetap masuk_se_userLogin'))

result = WebUI.getText(findTestObject('OLX/Page_OLX - Cara Tepat Jual Cepat/strong_fiqanadhira'))

WebUI.verifyMatch(result, 'fiqanadhira', false)

WebUI.closeBrowser()

