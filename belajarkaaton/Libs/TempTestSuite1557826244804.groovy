import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/TestSuiteExcel')

suiteProperties.put('name', 'TestSuiteExcel')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\asus\\Katalon Studio\\belajarkaaton\\Reports\\TestSuiteExcel\\20190514_163040\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/TestSuiteExcel', suiteProperties, [new TestCaseBinding('Test Cases/test_katalon6 - Iteration 1', 'Test Cases/test_katalon6',  [ 'status' : 'x' , 'user' : '' , 'message' : 'Login failed! Please ensure the username and password are valid.' , 'password' : '' ,  ]), new TestCaseBinding('Test Cases/test_katalon6 - Iteration 2', 'Test Cases/test_katalon6',  [ 'status' : 'x' , 'user' : '' , 'message' : 'Login failed! Please ensure the username and password are valid.' , 'password' : 'apassword' ,  ]), new TestCaseBinding('Test Cases/test_katalon6 - Iteration 3', 'Test Cases/test_katalon6',  [ 'status' : 'x' , 'user' : '' , 'message' : 'Login failed! Please ensure the username and password are valid.' , 'password' : 'ThisIsNotAPassword' ,  ]), new TestCaseBinding('Test Cases/test_katalon6 - Iteration 4', 'Test Cases/test_katalon6',  [ 'status' : 'x' , 'user' : 'john' , 'message' : 'Login failed! Please ensure the username and password are valid.' , 'password' : '' ,  ]), new TestCaseBinding('Test Cases/test_katalon6 - Iteration 5', 'Test Cases/test_katalon6',  [ 'status' : 'x' , 'user' : 'john' , 'message' : 'Login failed! Please ensure the username and password are valid.' , 'password' : 'apassword' ,  ]), new TestCaseBinding('Test Cases/test_katalon6 - Iteration 6', 'Test Cases/test_katalon6',  [ 'status' : 'x' , 'user' : 'john' , 'message' : 'Login failed! Please ensure the username and password are valid.' , 'password' : 'ThisIsNotAPassword' ,  ]), new TestCaseBinding('Test Cases/test_katalon6 - Iteration 7', 'Test Cases/test_katalon6',  [ 'status' : 'x' , 'user' : 'John Doe' , 'message' : 'Login failed! Please ensure the username and password are valid.' , 'password' : '' ,  ]), new TestCaseBinding('Test Cases/test_katalon6 - Iteration 8', 'Test Cases/test_katalon6',  [ 'status' : 'x' , 'user' : 'John Doe' , 'message' : 'Login failed! Please ensure the username and password are valid.' , 'password' : 'apassword' ,  ]), new TestCaseBinding('Test Cases/test_katalon6 - Iteration 9', 'Test Cases/test_katalon6',  [ 'status' : 'y' , 'user' : 'John Doe' , 'message' : 'Make Appointment' , 'password' : 'ThisIsNotAPassword' ,  ])])
