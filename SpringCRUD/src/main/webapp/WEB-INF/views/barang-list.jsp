<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<a href="addBarang">Tambah Data</a>
<br/>
<table>
	<thead>
		<tr>
		<th>ID Barang</th>
		<th>Nama Barang</th>
		<th>Harga Barang</th>	
		<th>NIK</th>	
		<th>Nama Person</th>		
		<th>Action</th>
		</tr>
	
	<c:choose>
		<c:when test="${empty listBarang}">
		No Data
		</c:when>
			<c:otherwise>
				<c:forEach var="item" items="${listBarang}">
				<tr>
					<td>${item.idBarang}</td>
					<td>${item.namaBarang}</td>
					<td>${item.hargaBarang }</td>
					<td>${item.persons.NIK }</td>
					<td>${item.persons.namaPerson }</td>
					<td><a href="barang-edit/${item.idBarang}">Edit</a>
					<a href="barang-delete/${item.idBarang}">Delete</a></td>			
				</tr>
				</c:forEach>
			</c:otherwise>
	</c:choose>
	
	</thead>
</table>
</body>
</html>
