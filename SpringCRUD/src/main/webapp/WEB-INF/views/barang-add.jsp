<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<div align="center">

		<h1>New Item</h1>
		<form:form action="saveBarang" method="post">
			<table>
				<tr>
					<td>ID Barang :</td>
					<td><input type="number" name="idBarang" required="required" /></td>
				</tr>

				<tr>
					<td>Nama Barang :</td>
					<td><input type="text" name="namaBarang" required="required" /></td>
				</tr>
				
				<tr>
					<td>Harga Barang :</td>
					<td><input type="number" name="hargaBarang" required="required" /></td>
				</tr>
				
				<tr>
					<td>NIK Person: </td>
					<td><select class="form-control" name="NIK">
						<c:forEach items="${persons}" var="person">
							<option value="${person.NIK}">${person.NIK} -
							${person.namaPerson }</option>
						</c:forEach>
					</select></td>
				</tr>
				
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Save"></td>
				</tr>
			</table>
		</form:form>

	</div>


</body>
</html>
