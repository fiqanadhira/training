<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<a href="addPerson">Tambah Data</a>
<br/>
<table>
	<thead>
		<tr>
		<th>NIK</th>
		<th>Nama</th>
		<th>Action</th>		
		</tr>
	
	<c:choose>
		<c:when test="${empty listPerson}">
		No Data
		</c:when>
			<c:otherwise>
				<c:forEach var="orang" items="${listPerson}">
				<tr>
					<td>${orang.NIK}</td>
					<td>${orang.namaPerson}</td>
					<td><a href="orang-edit/${orang.NIK}">Edit</a> <a href="orang-delete/${orang.NIK}">Delete</a></td>			
				</tr>
				</c:forEach>
			</c:otherwise>
	</c:choose>
	
	</thead>
</table>
</body>
</html>
