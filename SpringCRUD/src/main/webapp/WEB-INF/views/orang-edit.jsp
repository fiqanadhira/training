<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html ; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div align="center">
	<h1>EDIT PERSON</h1>
	<form:form action="${pageContext.request.contextPath}/orang-update" method="post" modelAttribute="person" >
	${Sukses}
	<table>

<tr>
			<td> NIK :</td>
			<td><form:input path="NIK" readonly="true"/></td>		
		</tr>

		<tr>
			<td> Nama :</td>
			<td><form:input path="namaPerson" required="required"/></td>	
			</tr>	
	
		<tr>
			<td colspan="2" align="center"><input type="submit" value="Update"><a href="${contextPath}/orang-list">Kembali</a></td>
		</tr>
		
		
	</table>
	
	</form:form>

</div>


</body>
</html>
