package com.springcrud.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "person")

public class Person {

	@Id
	private long NIK;
	
	@Column
	private String namaPerson;
	
	@OneToMany (mappedBy="persons", cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	private Set<Barang> bar;
	
	
	
	public long getNIK() {
		return NIK;
	}

	public void setNIK(long nIK) {
		NIK = nIK;
	}

	public Set<Barang> getBar() {
		return bar;
	}

	public void setBar(Set<Barang> bar) {
		this.bar = bar;
	}

	public String getNamaPerson() {
		return namaPerson;
	}

	public void setNamaPerson(String namaPerson) {
		this.namaPerson = namaPerson;
	}

}
