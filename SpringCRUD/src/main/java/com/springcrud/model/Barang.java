package com.springcrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table (name = "barang")

public class Barang {
	
	

	@Id
	private long idBarang;
	
	@Column
	private String namaBarang;
	
	@Column
	private double hargaBarang;

	@ManyToOne
	@JoinColumn(name="NIK", referencedColumnName="NIK", insertable = true, updatable = true)
	private Person persons;
	
	@Transient
	private long NIK;
	
	public long getNIK() {
		return NIK;
	}

	public void setNIK(long nIK) {
		NIK = nIK;
	}

	public void setPersons(Person persons) {
		this.persons = persons;
	}

	public Person getPersons() {
		return persons;
	}
	
	public long getIdBarang() {
		return idBarang;
	}
	public double getHargaBarang() {
		return hargaBarang;
	}

	public void setHargaBarang(double hargaBarang) {
		this.hargaBarang = hargaBarang;
	}

	public void setIdBarang(long idBarang) {
		this.idBarang = idBarang;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	
}
