package com.springcrud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.springcrud.model.Person;
import com.springcrud.service.PersonService;

@Controller
public class PersonController {
	
	@Autowired
	PersonService personService;
	// Method Add Person
	@RequestMapping(value = "/addPerson")
	public ModelAndView addBarang(Model model) {
		ModelAndView modelAndView = new ModelAndView("orang-add");

		try {

			Person orang = new Person();
			
			modelAndView.addObject("orang", orang);

		} catch (Exception e) {
			System.err.println("Error Add Person");
		}

		return modelAndView;

	}

	// Method Save Person
	@RequestMapping(value = "/savePerson", method = RequestMethod.POST)
	public ModelAndView saving(@ModelAttribute Person orang) {
		ModelAndView modelAndView = new ModelAndView("orang-list");

		try {

			if (orang != null) {

				personService.save(orang);			

				modelAndView.addObject("orang", orang);

				System.err.println("after save");
			} else {
				modelAndView.addObject("message", "Data tidak boleh kosong");
			}

		} catch (Exception e) {
			// TODO: handle exception

			System.err.println("controller /saveItem: " + e);
		}

		return new ModelAndView("redirect:/orang-list");
	}

	// Method Menampilkan List Person
	@RequestMapping(value = "/orang-list")
	public ModelAndView getAllData() {

		ModelAndView modelAndView = null;
		List<Person> orang = null;
		try {
			orang = new ArrayList<Person>();
			modelAndView = new ModelAndView("orang-list");

			orang = personService.getAll();

			modelAndView.addObject("listPerson", orang);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}

		return modelAndView;
	}

	// Method Delete Person
	@RequestMapping(value = "/orang-delete/{id}", method = RequestMethod.GET)
	public ModelAndView deleteBarang(@PathVariable(value = "id") Long id) {

		try {
			
			personService.delete(id);

		} catch (Exception e) {
			System.err.println("Error Delete Barang" + e);
		}

		return new ModelAndView("redirect:/orang-list");
	}

	// Method Edit Person
	@RequestMapping(value = "/orang-edit/{id}")
	public ModelAndView editPerson(@PathVariable(value = "id") Long id) {

		ModelAndView modelAndView = new ModelAndView("orang-edit");
		Person orang = null;
		try {

			orang = new Person();
			orang = personService.getById(id);
			modelAndView.addObject("person", orang);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("error reqMap controller orang-edit " + e);
		}
		
		return modelAndView;
	}

	
	//Method Update Person
	@RequestMapping(value = "/orang-update", method = RequestMethod.POST)
	public ModelAndView updateBarang(@ModelAttribute Person orang) {

		ModelAndView modelAndView = new ModelAndView("orang-edit");
		try {

			if (orang != null) {
				personService.update(orang);
			}
			modelAndView.addObject("Sukses", "Data berikut berhasil di update");
			modelAndView.addObject("orang", orang);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("controller / updatePerson:  " + e);
		}

		return new ModelAndView("redirect:/orang-list");
	}

}
