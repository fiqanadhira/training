package com.springcrud.service;



import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springcrud.dao.PersonDao;
import com.springcrud.model.Person;

@Transactional
@Service
public class PersonServiceImpl  implements PersonService 	 {

	@Autowired
	PersonDao personDao;

	public List<Person> getAll() {
		// TODO Auto-generated method stub

		List<Person> orng = null;
		try {

			orng = new ArrayList<Person>();
			orng = personDao.getAll();

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}

		return orng;
	}

	public Person getById(Long id) {
		// TODO Auto-generated method stub
		Person person = null;
		
		try {
			person = personDao.getById(id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return person;
	}

	public void delete(Long id) {
		// TODO Auto-generated method stub
		Person person = null;
		try {
			person = new Person();
			person = this.getById(id);
			if(person !=null)
			{
				personDao.delete(person);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}
		

	}

	public void update(Person entity) {
		// TODO Auto-generated method stub
		try {
			if(entity.getNamaPerson()!=null)
			{
				personDao.update(entity);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}

	}

	public void save(Person entity) {
		// TODO Auto-generated method stub

		try {
			
			
			if (entity.getNamaPerson() != null) {
				System.out.println("MASUK SAVE SERVICE");
				personDao.save(entity);
				System.out.println("SUDAH SAVE SERVICE");

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}

	}
}