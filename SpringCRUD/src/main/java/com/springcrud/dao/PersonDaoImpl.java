package com.springcrud.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.springcrud.model.Person;

@Repository
public class PersonDaoImpl extends AbstractDao implements PersonDao{
	@SuppressWarnings("unchecked")
	public List<Person> getAll() {
		// TODO Auto-generated method stub
		List<Person> orang = null;
		
		String sql = "From Person";
		
		try {			
			orang = new ArrayList<Person>();
			getSession().beginTransaction();
			Query query = getSession().createQuery(sql);
			orang = query.list();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
		}finally {
			getSession().close();
		}
		
		return orang;
	}

	public Person getById(Long id) {
		// TODO Auto-generated method stub
		Person orang = null;
		try {
			orang = new Person();
			getSession().beginTransaction();
			orang =   (Person) getSession().get(Person.class, id);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("getById personDaoImpl :" +e);
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
		
		return orang;
	}

	public void delete(Person entity) {
		// TODO Auto-generated method stub
		try {
			getSession().beginTransaction();
			getSession().delete(entity);
		
		
			
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			getSession().getTransaction().rollback();
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
		
	}

	public void update(Person update) {
		// TODO Auto-generated method stub
		
		try {
			getSession().beginTransaction();
			getSession().update(update);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			getSession().getTransaction().rollback();
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
		
	}

	public void save(Person entity) {
		// TODO Auto-generated method stub
		
		try {			
			getSession().beginTransaction();
			getSession().save(entity);
		} catch (Exception e) {
			// TODO: handle exception
			
			System.err.println(e);
			getSession().getTransaction().rollback();
			
		}finally {
			getSession().getTransaction().commit();
			getSession().close();
		}
	}
}
